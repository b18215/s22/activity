/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];
console.log("Original registeredUsers")
console.log(registeredUsers);

let friendsList = [];
console.log("Original friendsList");
console.log(friendsList);
/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    function verifyRegistration(checkToAdd) {
        
        let validate = registeredUsers.includes(checkToAdd);
        console.log(validate);
        if (validate === true) {
            console.warn("Registration failed. Username already exists!")
        } if (validate === false) {
            registeredUsers.push (checkToAdd);
            console.log(registeredUsers);
            console.log("Thank you for registering!");
         };   
    };
    verifyRegistration("Lester Alarcon");



/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
    function addFriend(username){

        let foundUser = registeredUsers.includes(username);
        if (foundUser === false) {
            console.warn("User not found.")
        } if (foundUser === true) {
            friendsList.push (username);
            console.warn("You have added "+''+ username +''+" as a friend!");
         };   
    };
    addFriend("Lester Alarcon");
    addFriend("James Jeffries");
    addFriend("Gunther Smith");
    addFriend("Macie West");

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
        function displayFriends(){
        console.log("Current friends List:");
        console.log(friendsList);
        friendsList.forEach(function(friends){
        });
        if (friendsList.length === 0){
        console.warn("You currently have "+''+ friendsList.length +''+" friends. Add one first.")
        
        }
        };
        displayFriends();
      
        
/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
        function displayNumberOfFriends(){
        friendsList.forEach(function(friends){
        });
        console.log("You currently have "+''+ friendsList.length +''+" friends.");
        if (friendsList.length === 0){
        console.warn("You currently have "+''+ friendsList.length +''+" friends. Add one first.")
        
        }
        
        };
        displayNumberOfFriends();


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
        function deleteFriend(){
            friendsList.pop();
            if (friendsList.length === 0){
            console.warn("You currently have "+''+ friendsList.length +''+" friends. Add one first.")
        };
        };
        deleteFriend();
        console.log(friendsList);

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

       function deleteByIndex(num1){
            friendsList.splice (num1, 1);
            console.log("Friends list");
            console.log(friendsList);
        };
        deleteByIndex(0);